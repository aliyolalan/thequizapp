# The QuizApp

The QuizApp is a progressive web app ([PWA](https://developers.google.com/web/progressive-web-apps)) built with [React](https://reactjs.org) using [OpenTDB](https://opentdb.com) API.


## Development

To get a local copy of the code, clone it using git:

```
git clone https://gitlab.com/aliyolalan/thequizapp
cd thequizapp
```

The QuizApp:

```
https://ali-yolalan-quiz-app.netlify.app/
```

Install dependencies:

```
npm install
```

Finally, you need to start a local web server. Run:

```
npm start
```

#### npm scripts

| Script        | Description                                                             |
| ------------- | ----------------------------------------------------------------------- |
| npm start     | Runs the app in the development mode.                                   |
| npm test      | Launches the test runner in the interactive watch mode.                 |
| npm run build | Builds the app for production to the `build` folder.                    |
| npm run eject | This command will remove the single build dependency from your project. |

