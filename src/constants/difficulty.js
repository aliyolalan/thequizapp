const DIFFICULTY = [
  {
    key: '0',
    text: 'Farketmez',
    value: '0'
  },
  {
    key: 'easy',
    text: 'Kolay',
    value: 'easy'
  },
  {
    key: 'medium',
    text: 'Orta',
    value: 'medium'
  },
  {
    key: 'hard',
    text: 'Zor',
    value: 'hard'
  }
];

export default DIFFICULTY;
