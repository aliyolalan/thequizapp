const QUESTIONS_TYPE = [
  {
    key: '0',
    text: 'Farketmez',
    value: '0'
  },
  {
    key: 'multiple',
    text: 'Çoktan Seçmeli',
    value: 'multiple'
  },
  {
    key: 'boolean',
    text: 'Doğru / Yanlış',
    value: 'boolean'
  }
];

export default QUESTIONS_TYPE;
