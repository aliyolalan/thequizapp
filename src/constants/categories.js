const CATEGORIES = [
  {
    key: '0',
    text: 'Farketmez',
    value: '0'
  },
  {
    key: '9',
    text: 'Genel Kültür',
    value: '9'
  },
  {
    key: '10',
    text: 'Eğlence: Kitaplar',
    value: '10'
  },
  {
    key: '11',
    text: 'Eğlence: Filmler',
    value: '11'
  },
  {
    key: '12',
    text: 'Eğlence: Muzik',
    value: '12'
  },
  {
    key: '13',
    text: 'Eğlence: Muzikal & Tiyatro',
    value: '13'
  },
  {
    key: '14',
    text: 'Eğlence: Televizyon',
    value: '14'
  },
  {
    key: '15',
    text: 'Eğlence: Video Oyunları',
    value: '15'
  },
  {
    key: '16',
    text: 'Eğlence: Masa Oyunları',
    value: '16'
  },
  {
    key: '17',
    text: 'Bilim & Doğa',
    value: '17'
  },
  {
    key: '18',
    text: 'Bilim: Bilgisayarlar',
    value: '18'
  },
  {
    key: '19',
    text: 'Bilim: Matematik',
    value: '19'
  },
  {
    key: '20',
    text: 'Mitoloji',
    value: '20'
  },
  {
    key: '21',
    text: 'Spor',
    value: '21'
  },
  {
    key: '22',
    text: 'Coğrafya',
    value: '22'
  },
  {
    key: '23',
    text: 'Tarih',
    value: '23'
  },
  {
    key: '24',
    text: 'Politika',
    value: '24'
  },
  {
    key: '25',
    text: 'Sanat',
    value: '25'
  },
  {
    key: '26',
    text: 'Ünlüler',
    value: '26'
  },
  {
    key: '27',
    text: 'Hayvanlar',
    value: '27'
  },
  {
    key: '28',
    text: 'Araçlar',
    value: '28'
  },
  {
    key: '29',
    text: 'Eğlence: Çizgi Romanlar',
    value: '29'
  },
  {
    key: '30',
    text: 'Bilim: İcatlar',
    value: '30'
  },
  {
    key: '31',
    text: 'Eğlence: Japon Anime & Manga',
    value: '31'
  },
  {
    key: '32',
    text: 'Eğlence: Çizgi Film & Animasyon',
    value: '32'
  }
];

export default CATEGORIES;
