import React, { Component } from 'react';
import {
  Container,
  Segment,
  Item,
  Dropdown,
  Divider,
  Button
} from 'semantic-ui-react';
import codeImg from '../../assets/images/code.png';

import {
  CATEGORIES,
  NUM_OF_QUESTIONS,
  DIFFICULTY,
  QUESTIONS_TYPE,
  COUNTDOWN_TIME
} from '../../constants';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      category: null,
      numOfQ: null,
      difficulty: null,
      type: null,
      time: null
    };

    this.setValue = this.setValue.bind(this);
  }

  setValue(name, value) {
    this.setState({ [name]: value });
  }

  render() {
    const { category, numOfQ, difficulty, type, time } = this.state;

    let allFieldsSelected = false;
    let selectedValues = null;

    if (category && numOfQ && difficulty && type && time) {
      allFieldsSelected = true;

      selectedValues = {
        category,
        numOfQ,
        difficulty,
        type,
        time
      };
    }

    return (
      <Container>
        <Segment>
          <Item.Group divided>
            <Item>
              <Item.Image src={codeImg} />
              <Item.Content>
                <Item.Header>
                  <h1>Open Trivia Soruları</h1>
                </Item.Header>
                <br />
                <Divider />
                <Item.Meta>
                  <Dropdown
                    fluid
                    selection
                    name="category"
                    placeholder="Quiz Kategorisi Seçin"
                    options={CATEGORIES}
                    onChange={(e, { name, value }) =>
                      this.setValue(name, value)
                    }
                  />
                  <br />
                  <Dropdown
                    fluid
                    selection
                    name="numOfQ"
                    placeholder="Soru Sayısını Seçin"
                    options={NUM_OF_QUESTIONS}
                    onChange={(e, { name, value }) =>
                      this.setValue(name, value)
                    }
                  />
                  <br />
                  <Dropdown
                    fluid
                    selection
                    name="difficulty"
                    placeholder="Zorluk Seviyesini Seçin"
                    options={DIFFICULTY}
                    onChange={(e, { name, value }) =>
                      this.setValue(name, value)
                    }
                  />
                  <br />
                  <Dropdown
                    fluid
                    selection
                    name="type"
                    placeholder="Soru Tipini Seçin"
                    options={QUESTIONS_TYPE}
                    onChange={(e, { name, value }) =>
                      this.setValue(name, value)
                    }
                  />
                  <br />
                  <Dropdown
                    fluid
                    selection
                    name="time"
                    placeholder="Süreyi Belirleyin (Dakika olarak)"
                    options={COUNTDOWN_TIME}
                    onChange={(e, { name, value }) =>
                      this.setValue(name, value)
                    }
                  />
                </Item.Meta>
                <Divider />
                <Item.Extra>
                  {allFieldsSelected ? (
                    <Button
                      primary
                      content="Quize Başlayın"
                      onClick={() => this.props.startQuiz(selectedValues)}
                      size="big"
                      icon="play"
                      labelPosition="left"
                    />
                  ) : (
                    <Button
                      disabled
                      primary
                      content="Quize Başlayın"
                      size="big"
                      icon="play"
                      labelPosition="left"
                    />
                  )}
                </Item.Extra>
              </Item.Content>
            </Item>
          </Item.Group>
        </Segment>
        <br />
      </Container>
    );
  }
}

export default Main;
