import React from 'react';
import { Segment, Header, Button } from 'semantic-ui-react';
import ShareButton from '../ShareButton';

import { calculateGrade } from '../../utils/calculateGrade';
import { timeConverter } from '../../utils/timeConverter';

const Stats = props => {
  const {
    totalQuestions,
    correctAnswers,
    timeTakesToComplete,
    retakeQuiz,
    backToHome
  } = props;

  const score = Number(((correctAnswers * 100) / totalQuestions).toFixed(2));
  const { grade, remarks } = calculateGrade(score);
  const { hours, minutes, seconds } = timeConverter(
    timeTakesToComplete.totalTime - timeTakesToComplete.timerTime
  );

  return (
    <Segment>
      <Header as="h1" textAlign="center" block>
        {remarks}
      </Header>
      <Header as="h2" textAlign="center" block>
        Derece: {grade}
      </Header>
      <Header as="h3" textAlign="center" block>
        Toplam Soru: {totalQuestions}
      </Header>
      <Header as="h3" textAlign="center" block>
        Doğru Cevaplar: {correctAnswers}
      </Header>
      <Header as="h3" textAlign="center" block>
        Skorunuz: {score}%
      </Header>
      <Header as="h3" textAlign="center" block>
        Geçme skoru: 60%
      </Header>
      <Header as="h3" textAlign="center" block>
        Harcanılan Zaman: {`${hours} : ${minutes} : ${seconds}`}
      </Header>
      <div style={{ marginTop: 35 }}>
        <Button
          primary
          content="Quizi Tekrarla"
          onClick={retakeQuiz}
          size="big"
          icon="redo"
          labelPosition="left"
          style={{ marginRight: 15, marginBottom: 8 }}
        />
        <Button
          color="teal"
          content="Anasayfa"
          onClick={backToHome}
          size="big"
          icon="home"
          labelPosition="left"
          style={{ marginBottom: 8 }}
        />
        <ShareButton />
      </div>
    </Segment>
  );
};

export default Stats;
