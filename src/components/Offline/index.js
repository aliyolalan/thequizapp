import React from 'react';
import { Container, Segment, Header, Icon } from 'semantic-ui-react';

const Offline = () => (
  <Container>
    <Segment placeholder>
      <Header icon>
        <Icon name="unlink" />
        <br />
        Lütfen internet bağlantınızı kontrol edin.
      </Header>
    </Segment>
  </Container>
);

export default Offline;
